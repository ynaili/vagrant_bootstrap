# A.J.'s Vagrant Bootstrap 2.0
A dead-simple LAMP stack without any bells and whistles for your basic Linux/Apache/MySQL/PHP install, using Chef Solo for provisioning.

## Requirements
* [VirtualBox](https://www.virtualbox.org)
* [Vagrant](http://vagrantup.com)
* [Berkshelf](http://berkshelf.com/)
	* `gem install berkshelf`
* [vagrant-berkshelf](https://github.com/riotgames/vagrant-berkshelf)
	* `vagrant plugin install vagrant-berkshelf`
* [vagrant-hostmanager](https://github.com/smdahlen/vagrant-hostmanager)
	* `vagrant plugin install vagrant-hostmanager`

## Setup
* Create repo for your project
* cd to project directory
* Run `git submodule add git@bitbucket.org:northps/vagrant_bootstrap.git`
* `cp vagrant_bootstrap/sample.Vagrantfile Vagrantfile`
* `cp vagrant_bootstrap/sample.Berksfile Berksfile`
* Update Vagrantfile and Berksfile for your project
* Run `vagrant up`

## Installed software
* Apache 2
* MySQL
* PHP 5.4 (with mysql, curl, mcrypt, memcached, gd)
* memcached
* postfix
* vim, git, screen, curl, composer