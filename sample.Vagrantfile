# -*- mode: ruby -*-
# vi: set ft=ruby :


# General project settings
#################################

  # IP Address for the host only network, change it to anything you like
  # but please keep it within the IPv4 private network range
  ip_address = "172.22.22.22"

  # The project name is base for directories, hostname and alike
  project_name = "projectname"

  # MySQL and PostgreSQL password - feel free to change it to something
  # more secure (Note: Changing this will require you to update the index.php example file)
  database_name = "databasename"
  database_password = "root"



# Vagrant configuration
#################################

  Vagrant.configure("2") do |config|
    
    # Configure vbox settings
    config.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--memory", "2048"]
      v.customize ["modifyvm", :id, "--cpus", "1"]
      v.customize ["modifyvm", :id, "--cpuexecutioncap", "85"]
    end

    # Enable Berkshelf support
    config.berkshelf.enabled = true

    # Define VM box to use
    config.vm.box = "precise64"
    config.vm.box_url = "http://files.vagrantup.com/precise64.box"

    # Set share folder
    config.vm.synced_folder "./", "/var/www/" + project_name + "/", type: "nfs"


    # Use hostonly network with a static IP Address and enable
    # hostmanager so we can have a custom domain for the server
    # by modifying the host machines hosts file
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.vm.define project_name do |node|
      node.vm.hostname = project_name + ".local"
      node.vm.network :private_network, ip: ip_address
      node.hostmanager.aliases = [ "www." + project_name + ".local" ]
    end
    config.vm.provision :hostmanager


    # Enable and configure chef solo
    config.vm.provision :chef_solo do |chef|
      chef.add_recipe "bootstrap::packages"
      chef.add_recipe "bootstrap::web_server"
      chef.add_recipe "bootstrap::vhost"
      chef.add_recipe "memcached"
      chef.add_recipe "bootstrap::db"
      chef.json = {
        :app => {
          # Project name
          :name           => project_name,

          # Name of MySQL database that should be created
          :db_name        => database_name,

          # Server name and alias(es) for Apache vhost
          :server_name    => project_name + ".local",
          :server_aliases =>  [ "www." + project_name + ".local" ],

          # Document root for Apache vhost
          :docroot        => "/var/www/" + project_name + "/docroot",

          # General packages
          :packages   => %w{ vim git screen curl },

          # PHP packages
          :php_packages   => %w{ php5-mysqlnd php5-curl php5-mcrypt php5-memcached php5-gd }
        },
        :mysql => {
          :server_root_password   => database_password,
          :server_repl_password   => database_password,
          :server_debian_password => database_password,
          :bind_address           => ip_address,
          :allow_remote_root      => true
        }
      }
    end
  end
